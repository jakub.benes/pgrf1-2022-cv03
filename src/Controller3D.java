
import model.Polygon;
import rasterize.LineRasterizer;
import rasterize.LineRasterizerGraphics;
import rasterize.PolygonRasterizer;
import rasterize.RasterBufferImage;
import render.WireRenderer;
import solids.Cube;
import solids.Solid;
import transforms.Mat4Transl;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Controller3D {
    private final JFrame frame;
    private final JPanel panel;
    private RasterBufferImage raster;
    private LineRasterizer lineRasterizer;
    private WireRenderer wireRenderer;

    // Scene
    private Solid cube;
    private Solid cube2;

    public Controller3D(int width, int height) {
        frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("PGRF1");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        raster = new RasterBufferImage(800, 600);
        lineRasterizer = new LineRasterizerGraphics(raster);
        wireRenderer = new WireRenderer(lineRasterizer);

        cube = new Cube();

        cube2 = new Cube();

        Mat4Transl mat4Transl = new Mat4Transl(300, -100, 0);
        cube2.setModel(mat4Transl);

        panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                raster.present(g);
            }
        };

        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

        panel.requestFocus();
        panel.requestFocusInWindow();
    }

    public void start() {
        raster.clear();

        List<Solid> scene = new ArrayList<>();
        scene.add(cube);
        scene.add(cube2);
        wireRenderer.renderScene(scene);

        panel.repaint();
    }
}
