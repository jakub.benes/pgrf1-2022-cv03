package render;

import model.Line;
import rasterize.LineRasterizer;
import solids.Solid;
import transforms.Point3D;

import java.util.List;

public class WireRenderer {
    private LineRasterizer lineRasterizer;

    public WireRenderer(LineRasterizer lineRasterizer) {
        this.lineRasterizer = lineRasterizer;
    }

    public void renderSolid(Solid solid) {
        for (int i = 0; i < solid.getIb().size(); i = i + 2) {
            int index1 = solid.getIb().get(i);
            int index2 = solid.getIb().get(i + 1);

            Point3D point1 = solid.getVb().get(index1);
            Point3D point2 = solid.getVb().get(index2);

            Point3D point1Trans = point1.mul(solid.getModel());
            Point3D point2Trans = point2.mul(solid.getModel());

            // TODO: Transformace do okna obrazovky


            Line line = new Line(
                    (int)Math.round(point1Trans.getX()), (int)Math.round(point1Trans.getY()),
                    (int)Math.round(point2Trans.getX()), (int)Math.round(point2Trans.getY())
            );
            lineRasterizer.rasterize(line);
        }
    }

    public void renderScene(List<Solid> solids) {
        for (Solid solid : solids) {
            renderSolid(solid);
        }
    }
}
